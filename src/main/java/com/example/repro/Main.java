package com.example.repro;

public class Main {

    // This bit of indirection is necessary so that I can use JavaFX on the
    // classpath rather than via the module path.
    public static void main(String[] args) {
        ExampleApp.main(args);
    }
}
