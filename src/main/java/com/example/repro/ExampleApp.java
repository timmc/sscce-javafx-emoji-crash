package com.example.repro;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ExampleApp extends Application {
    public void start(Stage primaryStage) {
        TextField textField = new TextField();
        textField.setText("Paste emoji here (e.g. U+1F4A5)"); // 💥
        primaryStage.setScene(new Scene(new StackPane(textField)));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
