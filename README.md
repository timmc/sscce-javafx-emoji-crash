# SSCCE for emoji crashing JavaFX

[SSCCE](http://sscce.org/ "Short, Self Contained, Correct, Example")
for https://github.com/javafxports/openjdk-jfx/issues/287

This crash has only been reproduced so far on Linux (including Arch,
Debian, and Fedora.)

Run:

```
mvn clean package
java -jar target/repro-1.0-SNAPSHOT-jar-with-dependencies.jar
```

Paste an emoji such as `💥` into the text field that appears.
The application will then freeze and start spewing stack traces like this:

```
java.lang.ArrayIndexOutOfBoundsException: Index -21 out of bounds for length 32
	at com.sun.prism.impl.GlyphCache.getCachedGlyph(GlyphCache.java:332)
	at com.sun.prism.impl.GlyphCache.render(GlyphCache.java:148)
	at com.sun.prism.impl.ps.BaseShaderGraphics.drawString(BaseShaderGraphics.java:2101)
	at com.sun.javafx.sg.prism.NGText.renderText(NGText.java:312)
	at com.sun.javafx.sg.prism.NGText.renderContent2D(NGText.java:270)
	at com.sun.javafx.sg.prism.NGShape.renderContent(NGShape.java:261)
	at com.sun.javafx.sg.prism.NGNode.doRender(NGNode.java:2072)
	at com.sun.javafx.sg.prism.NGNode.render(NGNode.java:1964)
	at com.sun.javafx.sg.prism.NGGroup.renderContent(NGGroup.java:270)
	at com.sun.javafx.sg.prism.NGRegion.renderContent(NGRegion.java:578)
	at com.sun.javafx.sg.prism.NGNode.renderForClip(NGNode.java:2313)
	at com.sun.javafx.sg.prism.NGNode.renderRectClip(NGNode.java:2207)
	at com.sun.javafx.sg.prism.NGNode.renderClip(NGNode.java:2233)
	at com.sun.javafx.sg.prism.NGNode.doRender(NGNode.java:2066)
	at com.sun.javafx.sg.prism.NGNode.render(NGNode.java:1964)
	at com.sun.javafx.sg.prism.NGGroup.renderContent(NGGroup.java:270)
	at com.sun.javafx.sg.prism.NGRegion.renderContent(NGRegion.java:578)
	at com.sun.javafx.sg.prism.NGNode.doRender(NGNode.java:2072)
	at com.sun.javafx.sg.prism.NGNode.render(NGNode.java:1964)
	at com.sun.javafx.sg.prism.NGGroup.renderContent(NGGroup.java:270)
	at com.sun.javafx.sg.prism.NGRegion.renderContent(NGRegion.java:578)
	at com.sun.javafx.sg.prism.NGNode.doRender(NGNode.java:2072)
	at com.sun.javafx.sg.prism.NGNode.render(NGNode.java:1964)
	at com.sun.javafx.tk.quantum.ViewPainter.doPaint(ViewPainter.java:479)
	at com.sun.javafx.tk.quantum.ViewPainter.paintImpl(ViewPainter.java:321)
	at com.sun.javafx.tk.quantum.PresentingPainter.run(PresentingPainter.java:91)
	at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:515)
	at java.base/java.util.concurrent.FutureTask.runAndReset(FutureTask.java:305)
	at com.sun.javafx.tk.RenderJob.run(RenderJob.java:58)
	at java.base/java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1128)
	at java.base/java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:628)
	at com.sun.javafx.tk.quantum.QuantumRenderer$PipelineRunnable.run(QuantumRenderer.java:125)
	at java.base/java.lang.Thread.run(Thread.java:834)
```
